export class CreateTaskDto {
  name: string;
  completed: boolean;
}
