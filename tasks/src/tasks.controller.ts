import { Body, Controller, Get, HttpStatus, UseFilters } from '@nestjs/common';
import { MessagePattern, RpcException } from '@nestjs/microservices';
import { CreateTaskDto } from './dto/create-task.dto';
import { AllExceptionsFilter } from './filters/all-exception.filter';
import { TasksService } from './tasks.service';
import { isValidObjectId, ObjectId } from 'mongoose';
import { UpdateTaskDto } from './dto/update-task.dto';

@Controller()
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('TASKS_CREATE')
  async create(@Body() createTaskDto: CreateTaskDto) {
    try {
      return await this.tasksService.create(createTaskDto);
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('TASKS_FIND_ALL')
  async findAll() {
    try {
      return await this.tasksService.findAll();
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('TASKS_FIND_BY_ID')
  async findOne(id: ObjectId) {
    try {
      if (!isValidObjectId(id)) {
        throw new RpcException({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          message: 'Bad object id',
        });
      }

      return await this.tasksService.findOne(id);
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('TASKS_UPDATE_BY_ID')
  async update(payload: { id: ObjectId; updateTaskDto: UpdateTaskDto }) {
    try {
      const { id, updateTaskDto } = payload;

      if (!isValidObjectId(id)) {
        throw new RpcException({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          message: 'Bad object id',
        });
      }

      // If whole request body is empty, throw error
      if (JSON.stringify(updateTaskDto) === '{}') {
        throw new RpcException({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          message: 'Please provide any field values to update',
        });
      }

      return await this.tasksService.update(id, updateTaskDto);
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('TASKS_REMOVE_BY_ID')
  async remove(id: ObjectId) {
    try {
      if (!isValidObjectId(id)) {
        throw new RpcException({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          message: 'Bad object id',
        });
      }

      return await this.tasksService.remove(id);
    } catch (error) {
      throw error;
    }
  }
}
