import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Task, TaskDocument } from './models/task.model';
import { Model, ObjectId } from 'mongoose';
import { CreateTaskDto } from './dto/create-task.dto';
import { RpcException } from '@nestjs/microservices';
import { UpdateTaskDto } from './dto/update-task.dto';

@Injectable()
export class TasksService {
  constructor(@InjectModel(Task.name) private taskModel: Model<TaskDocument>) {}

  async create(createTaskDto: CreateTaskDto) {
    try {
      const newTask = new this.taskModel(createTaskDto);

      return await newTask.save();
    } catch (error) {
      throw error;
    }
  }

  async findAll(): Promise<Task[]> {
    try {
      return await this.taskModel.find();
    } catch (error) {
      throw error;
    }
  }

  async findOne(id: ObjectId): Promise<Task> {
    try {
      const task = await this.taskModel.findById(id);

      if (!task) {
        throw new RpcException({
          status: HttpStatus.NOT_FOUND,
          message: 'Resource not found',
        });
      }

      return task;
    } catch (error) {
      throw error;
    }
  }

  async update(id: ObjectId, updateTaskDto: UpdateTaskDto) {
    try {
      await this.findOne(id);

      return await this.taskModel.findByIdAndUpdate(id, updateTaskDto);
    } catch (error) {
      throw error;
    }
  }

  async remove(id: ObjectId) {
    try {
      await this.findOne(id);

      return await this.taskModel.findByIdAndRemove(id);
    } catch (error) {
      throw error;
    }
  }
}
