import { Controller, HttpStatus, UseFilters } from '@nestjs/common';
import { MessagePattern, RpcException } from '@nestjs/microservices';
import { CreateUserDto } from './dto/create-user.dto';
import { AllExceptionsFilter } from './filters/all-exception.filter';
import { UsersService } from './users.service';
import { isValidObjectId, ObjectId } from 'mongoose';
import { UpdateUserDto } from './dto/update-user.dto';

@Controller()
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('USERS_CREATE')
  async create(createUserDto: CreateUserDto) {
    try {
      return await this.usersService.create(createUserDto);
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('USERS_FIND_ALL')
  async findAll() {
    try {
      return await this.usersService.findAll();
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('USERS_FIND_BY_ID')
  async findById(id: ObjectId) {
    try {
      if (!isValidObjectId(id)) {
        throw new RpcException({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          message: 'Bad object id',
        });
      }

      const user = await this.usersService.findById(id);

      if (!user) {
        throw new RpcException({
          status: HttpStatus.NOT_FOUND,
          message: 'Resource not found',
        });
      }

      return user;
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('USERS_FIND_BY_EMAIL')
  async findByEmail(email: string) {
    try {
      return await this.usersService.findByEmail(email);
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('USERS_UPDATE_BY_ID')
  async update(payload: { id: ObjectId; updateUserDto: UpdateUserDto }) {
    try {
      const { id, updateUserDto } = payload;

      if (!isValidObjectId(id)) {
        throw new RpcException({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          message: 'Bad object id',
        });
      }

      // If whole request body is empty, throw error
      if (JSON.stringify(updateUserDto) === '{}') {
        throw new RpcException({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          message: 'Please provide any field values to update',
        });
      }

      return await this.usersService.update(id, updateUserDto);
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('USERS_REMOVE_BY_ID')
  async remove(id: ObjectId) {
    try {
      if (!isValidObjectId(id)) {
        throw new RpcException({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          message: 'Bad object id',
        });
      }

      return await this.usersService.remove(id);
    } catch (error) {
      throw error;
    }
  }
}
