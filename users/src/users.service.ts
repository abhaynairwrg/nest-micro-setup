import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './models/user.model';
import { Model, ObjectId } from 'mongoose';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async create(createNoteDto: CreateUserDto) {
    try {
      const newUser = new this.userModel(createNoteDto);

      return await newUser.save();
    } catch (error) {
      throw error;
    }
  }

  async findAll(): Promise<User[]> {
    try {
      return await this.userModel.find({}, { password: 0 });
    } catch (error) {
      throw error;
    }
  }

  async findById(id: ObjectId): Promise<User> {
    try {
      const user = await this.userModel.findById(id);

      return user;
    } catch (error) {
      throw error;
    }
  }

  async findByEmail(email: string): Promise<User> {
    try {
      const user = await this.userModel.findOne({ email });

      return user;
    } catch (error) {
      throw error;
    }
  }

  async update(id: ObjectId, updateUserDto: UpdateUserDto) {
    try {
      return await this.userModel.findByIdAndUpdate(id, updateUserDto);
    } catch (error) {
      throw error;
    }
  }

  async remove(id: ObjectId) {
    try {
      return await this.userModel.findByIdAndRemove(id);
    } catch (error) {
      throw error;
    }
  }
}
