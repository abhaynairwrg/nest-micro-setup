export class CreateUserDto {
  name: string;
  email: string;
  role: string;
  password: string;
}
