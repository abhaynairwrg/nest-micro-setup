import { PartialType } from '@nestjs/mapped-types';
import { IsEnum, IsOptional, IsString } from 'class-validator';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends PartialType(CreateUserDto) {
  @IsString()
  @IsOptional()
  name: string;

  @IsEnum(['ADMIN', 'USER'], {
    message: "role should be either 'ADMIN' or 'USER'",
  })
  @IsOptional()
  role: string;
}
