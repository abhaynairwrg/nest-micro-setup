import { HttpException, Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { catchError, throwError } from 'rxjs';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UsersService {
  constructor(@Inject('USERS_SERVICE') private _usersClient: ClientProxy) {}

  create(createUserDto: CreateUserDto) {
    return this._usersClient
      .send('USERS_CREATE', createUserDto)
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  findAll() {
    return this._usersClient
      .send('USERS_FIND_ALL', {})
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  findById(id: string) {
    return this._usersClient
      .send('USERS_FIND_BY_ID', id)
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  findByEmail(email: string) {
    return this._usersClient
      .send('USERS_FIND_BY_EMAIL', email)
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  update(id: string, userData) {
    return this._usersClient
      .send('USERS_UPDATE_BY_ID', {
        id,
        userData,
      })
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  remove(id: string) {
    return this._usersClient
      .send('USERS_REMOVE_BY_ID', id)
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }
}
