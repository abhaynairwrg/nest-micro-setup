import {
  Body,
  ConflictException,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Res,
} from '@nestjs/common';
import { Public } from 'src/auth/role/public.decorator';
import { CreateUserDto } from './dto/create-user.dto';
import { UsersService } from './users.service';
import { hash } from 'bcryptjs';
import { lastValueFrom } from 'rxjs';
import { Response } from 'express';
import { Roles } from 'src/auth/role/role.decorator';
import { Role } from 'src/auth/role/role.enum';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Public()
  @Post()
  async create(@Body() createUserDto: CreateUserDto, @Res() res: Response) {
    try {
      const { password } = createUserDto;

      const hashedPassword = await hash(password, 10);

      const userExists = await lastValueFrom(
        this.usersService.findByEmail(createUserDto.email),
      );

      if (userExists) {
        throw new ConflictException('Email already exists');
      }

      await lastValueFrom(
        this.usersService.create({
          ...createUserDto,
          password: hashedPassword,
        }),
      );

      return res.status(201).send('Created User');
    } catch (error) {
      throw error;
    }
  }

  @Roles(Role.ADMIN)
  @Get()
  findAll() {
    try {
      return this.usersService.findAll();
    } catch (error) {
      throw error;
    }
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() body) {
    try {
      return this.usersService.update(id, body);
    } catch (error) {
      throw error;
    }
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    try {
      return this.usersService.remove(id);
    } catch (error) {
      throw error;
    }
  }
}
