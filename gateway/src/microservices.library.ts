import { ClientsModuleOptions, Transport } from '@nestjs/microservices';

export default [
  {
    name: 'NOTES_SERVICE',
    transport: Transport.TCP,
    options: {
      host: process.env.NOTES_HOST,
      port: Number(process.env.NOTES_PORT),
    },
  },
  {
    name: 'USERS_SERVICE',
    transport: Transport.TCP,
    options: {
      host: process.env.USERS_HOST,
      port: Number(process.env.USERS_PORT),
    },
  },
  {
    name: 'TOKEN_SERVICE',
    transport: Transport.TCP,
    options: {
      host: process.env.TOKEN_HOST,
      port: Number(process.env.TOKEN_PORT),
    },
  },
  {
    name: 'TASKS_SERVICE',
    transport: Transport.TCP,
    options: {
      host: process.env.TASKS_HOST,
      port: Number(process.env.TASKS_PORT),
    },
  },
] as ClientsModuleOptions;
