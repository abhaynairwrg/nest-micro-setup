import { HttpException, Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { catchError, throwError } from 'rxjs';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';

@Injectable()
export class TasksService {
  constructor(@Inject('TASKS_SERVICE') private _tasksClient: ClientProxy) {}

  create(createTaskDto: CreateTaskDto) {
    return this._tasksClient
      .send('TASKS_CREATE', createTaskDto)
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  findAll() {
    return this._tasksClient
      .send('TASKS_FIND_ALL', {})
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  findOne(id: string) {
    return this._tasksClient
      .send('TASKS_FIND_BY_ID', id)
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  update(id: string, updateTaskDto: UpdateTaskDto) {
    return this._tasksClient
      .send('TASKS_UPDATE_BY_ID', {
        id,
        updateTaskDto,
      })
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  remove(id: string) {
    return this._tasksClient
      .send('TASKS_REMOVE_BY_ID', id)
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }
}
