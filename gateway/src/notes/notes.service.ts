import { HttpException, Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { catchError, throwError } from 'rxjs';
import { CreateNoteDto } from './dto/create-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';

@Injectable()
export class NotesService {
  constructor(@Inject('NOTES_SERVICE') private _notesClient: ClientProxy) {}

  create(createNoteDto: CreateNoteDto) {
    return this._notesClient
      .send('NOTES_CREATE', createNoteDto)
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  findAll() {
    return this._notesClient
      .send('NOTES_FIND_ALL', {})
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  findOne(id: string) {
    return this._notesClient
      .send('NOTES_FIND_BY_ID', id)
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  update(id: string, updateNoteDto: UpdateNoteDto) {
    return this._notesClient
      .send('NOTES_UPDATE_BY_ID', {
        id,
        updateNoteDto,
      })
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  remove(id: string) {
    return this._notesClient
      .send('NOTES_REMOVE_BY_ID', id)
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }
}
