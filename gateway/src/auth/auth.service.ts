import { Injectable, NotFoundException } from '@nestjs/common';
import { compare } from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';
import { lastValueFrom } from 'rxjs';
import { UsersService } from 'src/users/users.service';
import { TokenService } from './token.service';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private userService: UsersService,
    private tokenService: TokenService,
  ) {}

  async validateUser(email: string, password: string) {
    try {
      const user = await lastValueFrom(this.userService.findByEmail(email));

      if (!user) {
        throw new NotFoundException(
          'User not found, might be wrong credentials',
        );
      }

      const passwordsAreSame = await compare(password, user.password);

      if (user && passwordsAreSame) {
        const { password: userPass, ...rest } = user;

        return rest;
      }

      return null;
    } catch (error) {
      throw error;
    }
  }

  async login(user: any) {
    try {
      const payload = { email: user.email, sub: user._id, role: user.role };

      const access_token = this.jwtService.sign(payload);

      await lastValueFrom(
        this.tokenService.create({
          userId: user._id,
          token: access_token,
        }),
      );

      return {
        access_token,
      };
    } catch (error) {
      throw error;
    }
  }

  async logout(userId: string) {
    try {
      await lastValueFrom(this.tokenService.remove(userId));
    } catch (error) {
      throw error;
    }
  }
}
