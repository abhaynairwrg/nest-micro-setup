import { ForbiddenException, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Request } from 'express';
import { ExtractJwt, Strategy, StrategyOptions } from 'passport-jwt';
import { catchError, lastValueFrom, of } from 'rxjs';
import { TokenService } from '../token.service';

@Injectable()
export class JWTStrategy extends PassportStrategy(Strategy) {
  constructor(private tokenService: TokenService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.AUTH_JWT_SECRET,
      passReqToCallback: true,
    } as StrategyOptions);
  }

  async validate(req: Request, payload: any) {
    try {
      const rawToken = req.headers['authorization'].split(' ')[1];

      const tokenExists = await lastValueFrom(
        this.tokenService
          .findByToken(rawToken)
          .pipe(catchError((_) => of(false))),
      );

      if (!tokenExists) {
        throw new ForbiddenException('Token Expired');
      }

      return {
        _id: payload.sub,
        email: payload.email,
        role: payload.role,
      };
    } catch (error) {
      throw error;
    }
  }
}
