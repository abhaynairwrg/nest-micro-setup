import { HttpException, Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { catchError, throwError } from 'rxjs';
import { CreateTokenDto } from './dto/create-token.dto';

@Injectable()
export class TokenService {
  constructor(@Inject('TOKEN_SERVICE') private _tokenClient: ClientProxy) {}

  create(createTokenDto: CreateTokenDto) {
    return this._tokenClient
      .send('TOKEN_CREATE', createTokenDto)
      .pipe(
        catchError((err) =>
          throwError(
            () => new HttpException(err.message, Number(err.status) || 500),
          ),
        ),
      );
  }

  findById(userId: string) {
    return this._tokenClient
      .send('TOKEN_FIND_BY_USER_ID', userId)
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  findByToken(token: string) {
    return this._tokenClient
      .send('TOKEN_FIND_BY_TOKEN', token)
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }

  remove(userId: string) {
    return this._tokenClient
      .send('TOKEN_REMOVE_BY_USER_ID', userId)
      .pipe(
        catchError((err) =>
          throwError(() => new HttpException(err.message, err.status)),
        ),
      );
  }
}
