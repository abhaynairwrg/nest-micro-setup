import { Global, Module } from '@nestjs/common';
import { ClientsModule } from '@nestjs/microservices';
import microservicesLibrary from 'src/microservices.library';

@Global()
@Module({
  imports: [ClientsModule.register(microservicesLibrary)],
  exports: [ClientsModule],
})
export class MicroModule {}
