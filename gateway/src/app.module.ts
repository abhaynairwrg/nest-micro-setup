import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { ClientsModule } from '@nestjs/microservices';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { JwtAuthGuard } from './auth/guards/jwt.guard';
import { RoleGuard } from './auth/guards/role.guard';
import { NotesController } from './notes/notes.controller';
import { UsersController } from './users/users.controller';
import { UsersService } from './users/users.service';
import { AuthModule } from './auth/auth.module';
import { MicroModule } from './micro/micro.module';
import { NotesService } from './notes/notes.service';
import { ConfigModule } from '@nestjs/config';
import { TasksController } from './tasks/tasks.controller';
import { TasksService } from './tasks/tasks.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '../.env',
      ignoreEnvFile: process.env.NODE_ENV === 'development' ? true : false,
    }),
    MicroModule,
    AuthModule,
  ],
  controllers: [
    AppController,
    NotesController,
    UsersController,
    TasksController,
  ],
  providers: [
    AppService,
    UsersService,
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: RoleGuard,
    },
    NotesService,
    TasksService,
  ],
})
export class AppModule {}
