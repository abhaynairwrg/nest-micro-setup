import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { TokenModule } from './token.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    TokenModule,
    {
      transport: Transport.TCP,
      options: {
        host: process.env.TOKEN_HOST,
        port: Number(process.env.TOKEN_PORT),
      },
    },
  );

  app.listen();
}
bootstrap();
