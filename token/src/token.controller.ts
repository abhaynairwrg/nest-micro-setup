import { Body, Controller, HttpStatus, UseFilters } from '@nestjs/common';
import { MessagePattern, RpcException } from '@nestjs/microservices';
import { CreateTokenDto } from './dto/create-token.dto';
import { AllExceptionsFilter } from './filters/all-exception.filter';
import { TokenService } from './token.service';
import { ObjectId, isValidObjectId } from 'mongoose';

@Controller()
export class TokenController {
  constructor(private readonly tokenService: TokenService) {}

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('TOKEN_CREATE')
  async create(@Body() createTokenDto: CreateTokenDto) {
    try {
      return await this.tokenService.create(createTokenDto);
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('TOKEN_FIND_BY_USER_ID')
  async findByUserId(userId: ObjectId) {
    try {
      if (!isValidObjectId(userId)) {
        throw new RpcException({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          message: 'Bad object id',
        });
      }

      return await this.tokenService.findOne('userId', userId);
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('TOKEN_FIND_BY_TOKEN')
  async findByToken(token: string) {
    try {
      return await this.tokenService.findOne('token', token);
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('TOKEN_REMOVE_BY_USER_ID')
  async remove(userId: ObjectId) {
    try {
      if (!isValidObjectId(userId)) {
        throw new RpcException({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          message: 'Bad object id',
        });
      }

      return await this.tokenService.remove(userId);
    } catch (error) {
      throw error;
    }
  }
}
