import { HttpStatus, Injectable } from '@nestjs/common';
import { RpcException } from '@nestjs/microservices';
import { InjectModel } from '@nestjs/mongoose';
import { ObjectId, Model } from 'mongoose';
import { CreateTokenDto } from './dto/create-token.dto';
import { Token, TokenDocument } from './models/token.model';

@Injectable()
export class TokenService {
  constructor(
    @InjectModel(Token.name) private tokenModel: Model<TokenDocument>,
  ) {}

  async create(createTokenDto: CreateTokenDto) {
    try {
      // delete old tokens if any
      await this.tokenModel.deleteMany({ userId: createTokenDto.userId });

      const newToken = new this.tokenModel(createTokenDto);

      return await newToken.save();
    } catch (error) {
      throw error;
    }
  }

  async findOne(
    key: 'userId' | 'token',
    value: string | ObjectId,
  ): Promise<Token> {
    try {
      const token = await this.tokenModel.findOne({ [key]: value });

      if (!token) {
        throw new RpcException({
          status: HttpStatus.NOT_FOUND,
          message: 'Resource not found',
        });
      }

      return token;
    } catch (error) {
      throw error;
    }
  }

  async remove(userId: ObjectId) {
    try {
      return await this.tokenModel.findOneAndRemove({ userId });
    } catch (error) {
      throw error;
    }
  }
}
