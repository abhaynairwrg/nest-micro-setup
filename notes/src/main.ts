import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { NotesModule } from './notes.module';

async function bootstrap() {
  const app = await NestFactory.createMicroservice<MicroserviceOptions>(
    NotesModule,
    {
      transport: Transport.TCP,
      options: {
        host: process.env.NOTES_HOST,
        port: Number(process.env.NOTES_PORT),
      },
    },
  );

  app.listen();
}
bootstrap();
