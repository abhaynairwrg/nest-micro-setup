import { Body, Controller, HttpStatus, UseFilters } from '@nestjs/common';
import { MessagePattern, RpcException } from '@nestjs/microservices';
import { isValidObjectId, ObjectId } from 'mongoose';
import { CreateNoteDto } from './dto/create-note.dto';
import { UpdateNoteDto } from './dto/update-note.dto';
import { AllExceptionsFilter } from './filters/all-exception.filter';
import { NotesService } from './notes.service';

@Controller()
export class NotesController {
  constructor(private readonly notesService: NotesService) {}

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('NOTES_CREATE')
  async create(@Body() createNoteDto: CreateNoteDto) {
    try {
      return await this.notesService.create(createNoteDto);
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('NOTES_FIND_ALL')
  async findAll() {
    try {
      return await this.notesService.findAll();
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('NOTES_FIND_BY_ID')
  async findOne(id: ObjectId) {
    try {
      if (!isValidObjectId(id)) {
        throw new RpcException({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          message: 'Bad object id',
        });
      }

      return await this.notesService.findOne(id);
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('NOTES_UPDATE_BY_ID')
  async update(payload: { id: ObjectId; updateNoteDto: UpdateNoteDto }) {
    try {
      const { id, updateNoteDto } = payload;

      if (!isValidObjectId(id)) {
        throw new RpcException({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          message: 'Bad object id',
        });
      }

      // If whole request body is empty, throw error
      if (JSON.stringify(updateNoteDto) === '{}') {
        throw new RpcException({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          message: 'Please provide any field values to update',
        });
      }

      return await this.notesService.update(id, updateNoteDto);
    } catch (error) {
      throw error;
    }
  }

  @UseFilters(new AllExceptionsFilter())
  @MessagePattern('NOTES_REMOVE_BY_ID')
  async remove(id: ObjectId) {
    try {
      if (!isValidObjectId(id)) {
        throw new RpcException({
          status: HttpStatus.UNPROCESSABLE_ENTITY,
          message: 'Bad object id',
        });
      }

      return await this.notesService.remove(id);
    } catch (error) {
      throw error;
    }
  }
}
